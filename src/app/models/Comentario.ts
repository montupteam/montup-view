import { Montanha } from './montanha';
import { Usuario } from './usuario';
import { ImagemMontanha } from './imagemMontanha';

export class Comentario {
    id: number;
    imagemMontanha: ImagemMontanha;
    idUsuario?: number;
    idMontanha?: number;
    comentarioTexto: string;
    dataComentario: Date;
    dataCriacao?: Date;
    registroAtivo: boolean;
    montanha: Montanha;
    usuario: Usuario;
}
