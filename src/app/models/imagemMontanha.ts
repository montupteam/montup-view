import { Montanha } from './montanha';
import { Comentario } from './Comentario';

export class ImagemMontanha {
    id: number;
    idMontanha?: number;
    montanha: Montanha[];
    comentario: Comentario[];
    urlImagem: string;
    urlImagemHash: string;
    registroAtivo?: boolean;
    dataCriacao?: Date;
}
