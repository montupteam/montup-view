import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fuseAnimations } from '@fuse/animations';
import { Usuario } from 'app/models/usuario';
import { UsuarioService } from 'app/services/usuario-meu-perfil.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { environment } from 'environments/environment';
import { Router } from '@angular/router';
import { Guia } from 'app/models/guia';
import { GuiaService } from 'app/services/guia.service';

const url = environment.baseUrl + '/usuario';

@Component({
    selector: 'usuario-meu-perfil',
    templateUrl: './usuario-meu-perfil.component.html',
    styleUrls: ['./usuario-meu-perfil.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class UsuarioMeuPerfilComponent {
    usuario: Usuario = new Usuario();
    usuarioArquivo: File;
    urlImagem: string;

    guia: Guia = new Guia();
    guias: Guia[];

    constructor(
        private usuarioservice: UsuarioService,
        public dialog: MatDialog,
        public http: HttpClient,
        private guiaService: GuiaService

    ) { }

    openInsertImageDialog() {
        const dialogRef = this.dialog.open(AppInsertImageDialog);

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }

    ngOnInit() {
        this.usuarioservice.obterUsuarioLogado().subscribe((
            _usuario: Usuario) => {
            this.usuario = _usuario;
            if (this.usuario.imagemHash) {
                this.urlImagem =  environment.baseUrl + `/usuario/obterimagemusuario?id=${this.usuario.id}`;
            } else {
                this.urlImagem = `assets/images/avatars/profile.jpg`;
            }
        }, error => {
            console.log(error);
        });

        this.guiaService.obterTodosGuia().subscribe(x => {
            this.guias = x;
        });
    }

    getuserName() {
        return sessionStorage.getItem('username');
    }
}

@Component({
    selector: 'app-insert-image-dialog',
    templateUrl: './app-insert-image-dialog.html',
})
export class AppInsertImageDialog {
    constructor(
        public dialog: MatDialog,
        public http: HttpClient,
        private usuarioservice: UsuarioService,
        public router: Router,
        public dialogRef: MatDialogRef<AppInsertImageDialog>
    ) {

    }

    @ViewChild('profileImage', { static: false }) input: ElementRef;

    fechar(): void {
        this.dialogRef.close();
    }

    uploadImagem() {
        let fileList: FileList = this.input.nativeElement.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];

            this.usuarioservice.uploadImagem(file).subscribe(() => window.top.location.reload(), () => console.log('Falha ao fazer upload.'));
        }
    }
}