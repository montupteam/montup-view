import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Usuario } from 'app/models/usuario';
import { UsuarioService } from 'app/services/usuario-meu-perfil.service';
import { Router } from '@angular/router';
import { Guia } from 'app/models/guia';
import { GuiaService } from 'app/services/guia.service';

@Component({
    selector: 'usuario-guia-alterar',
    templateUrl: './usuario-guia-alterar.component.html',
    styleUrls: ['./usuario-guia-alterar.component.scss']
})

export class AlterarGuiaComponent implements OnInit, OnDestroy {
    usuarios: Usuario[];
    usuario: Usuario;
    guia: Guia = new Guia();

    alterarUsuarioGuiaFormEtapa1: FormGroup;
    alterarUsuarioGuiaFormEtapa2: FormGroup;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _formBuilder: FormBuilder,
        private usuarioService: UsuarioService,
        private guiaService: GuiaService,
        public router: Router
    ) {
        
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.alterarUsuarioGuiaFormEtapa1 = this._formBuilder.group({
            descricaoGuia: ['', Validators.required],
            ocupacao: ['', Validators.required],
            descricaoExperiencia: ['', Validators.required]
        });

        this.alterarUsuarioGuiaFormEtapa2 = this._formBuilder.group({
            codigoPostal: ['', Validators.required],
            telefone: ['', Validators.required],
            instagram: ['', Validators.required]
        });

        this.obterPeloId();
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    finishHorizontalStepper(): void {
        alert('You have finished the horizontal stepper!');
    }

    finishVerticalStepper(): void {
        alert('Alterado com sucesso');
    }

    obterTodos() {
        this.usuarioService.obterTodos().subscribe(
            (x: Usuario[]) => {
                this.usuarios = x;
            }, error => {
                console.log(error);
            });
    }

    obterPeloId() {
        this.usuarioService.obterUsuarioLogado().subscribe(
            (x: Usuario) => {
                this.usuario = x;
                this.alterarUsuarioGuiaFormEtapa1.setValue({
                    descricaoGuia: this.usuario.guia.descricaoGuia,
                    ocupacao: this.usuario.guia.ocupacao,
                    descricaoExperiencia: this.usuario.guia.descricaoExperiencia
                }),
                this.alterarUsuarioGuiaFormEtapa2.setValue({
                    codigoPostal: this.usuario.guia.codigoPostal,
                    telefone: this.usuario.guia.telefone,
                    instagram: this.usuario.guia.instagram
                });
            }, error => {
                console.log(error);
            });
    }

    salvarAlteracao() {
        debugger;
        this.usuario.guia.descricaoGuia = this.alterarUsuarioGuiaFormEtapa1.get('descricaoGuia').value;
        this.usuario.guia.ocupacao = this.alterarUsuarioGuiaFormEtapa1.get('ocupacao').value;
        this.usuario.guia.descricaoExperiencia = this.alterarUsuarioGuiaFormEtapa1.get('descricaoExperiencia').value;
        this.usuario.guia.codigoPostal = this.alterarUsuarioGuiaFormEtapa2.get('codigoPostal').value;
        this.usuario.guia.telefone = this.alterarUsuarioGuiaFormEtapa2.get('telefone').value;
        this.usuario.guia.instagram = this.alterarUsuarioGuiaFormEtapa2.get('instagram').value;

        this.guiaService.alterarGuia(this.usuario.guia).subscribe(
            () => {
                window.top.location.reload();
            }, error => {
                console.log(error);
            });
    }
}