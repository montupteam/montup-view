import { Component, OnInit, ViewEncapsulation, OnDestroy, Input } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { Usuario } from 'app/models/usuario';
import { UsuarioService } from 'app/services/usuario-meu-perfil.service';
import * as moment from 'moment';
import { Guia } from 'app/models/guia';

@Component({
    selector: 'usuario-informacoes',
    templateUrl: './usuario-informacoes.component.html',
    styleUrls: ['./usuario-informacoes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class UsuarioInformacoesComponent implements OnInit {

    @Input() usuario: Usuario;
    @Input() dataNascimentoUsuario: string;
    @Input() id: number;
    guia: Guia = new Guia();
    about: any;

    @Input() idUsuario: number;

    guias: Guia[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _profileService
     */
    constructor(private usuarioservice: UsuarioService) { }

    ngOnInit() {
       this.obterPeloId();
    }

    obterPeloId() {
        this.usuarioservice.obterUsuarioLogado().subscribe((
            _usuario: Usuario) => {
            this.usuario = _usuario;

            moment.locale('pt-BR');
            const t = moment(this.usuario.dataNascimento, 'DD/MM/YYYY HH:mm:ss');
            
            if(this.usuario.dataNascimento){
                this.dataNascimentoUsuario = t.toDate().toLocaleDateString();
            }
        }, error => {
            console.log(error);
        });
    }

    obterPeloIdUsuario() {
        this.usuarioservice.obterPeloIdUsuario(this.id).subscribe(
            (x: Usuario) => {
                this.usuario = x;
            });
        }    
}
