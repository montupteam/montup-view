import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Usuario } from 'app/models/usuario';
import { UsuarioService } from 'app/services/usuario-meu-perfil.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'usuario-alterar',
    templateUrl: './usuario-alterar.component.html',
    styleUrls: ['./usuario-alterar.component.scss']
})

export class AlterarComponent implements OnInit, OnDestroy {
    maxDate: Date = new Date();
    usuarios: Usuario[];
    usuario: Usuario;
    senha: string;
    novaSenha: string;
    alterarUsuarioFormEtapa1: FormGroup;
    alterarUsuarioFormEtapa2: FormGroup;
    alterarUsuarioFormEtapa3: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _formBuilder: FormBuilder,
        private usuarioService: UsuarioService,
        public router: Router
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit(): void {
        // Vertical Stepper form stepper
        this.alterarUsuarioFormEtapa1 = this._formBuilder.group({
            username: ['', Validators.required],
            sobrenome: ['', Validators.required]
        });

        this.alterarUsuarioFormEtapa2 = this._formBuilder.group({
            email: ['', Validators.required]
        });

        this.alterarUsuarioFormEtapa3 = this._formBuilder.group({
            cpf: ['', Validators.required],
            dataNascimento: ['', Validators.required],
        });
        

        this.obterPeloId();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Finish the horizontal stepper
     */
    finishHorizontalStepper(): void {
        alert('You have finished the horizontal stepper!');
    }

    /**.
     * Finish the vertical stepper
     */
    finishVerticalStepper(): void {
        alert('Alterado com sucesso');
    }

    obterTodos() {
        this.usuarioService.obterTodos().subscribe(
            (x: Usuario[]) => {
                this.usuarios = x;
            }, error => {
                console.log(error);
            });
    }

    obterPeloId() {
        this.usuarioService.obterUsuarioLogado().subscribe(
            (x: Usuario) => {
                this.usuario = x;
                this.alterarUsuarioFormEtapa1.setValue({
                    username: this.usuario.userName,
                    sobrenome: this.usuario.sobrenome,
                }),
                this.alterarUsuarioFormEtapa2.setValue({
                    email: this.usuario.email,
                }),
                this.alterarUsuarioFormEtapa3.setValue({
                    cpf: this.usuario.cpf,
                    dataNascimento: this.usuario.dataNascimento
                });
            }, error => {
                console.log(error);
            });
    }


    salvarAlteracao() {
        this.usuario = new Usuario();
        this.usuario.userName = this.alterarUsuarioFormEtapa1.get('username').value;
        this.usuario.sobrenome = this.alterarUsuarioFormEtapa1.get('sobrenome').value;
        this.usuario.email = this.alterarUsuarioFormEtapa2.get('email').value;
        this.usuario.cpf = this.alterarUsuarioFormEtapa3.get('cpf').value;
        this.usuario.dataNascimento = this.alterarUsuarioFormEtapa3.get('dataNascimento').value;

        this.usuarioService.alterarUsuario(this.usuario).subscribe(
            () => {
                // localStorage.removeItem('token');
                // this.router.navigate(['/autenticacao/login']);
                window.top.location.reload();
            }, error => {
                console.log(error);
            });
    }
}
