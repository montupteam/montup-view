import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioPerfilComponent } from './usuario-perfil/usuario-perfil.component';
import { UsuarioMeuPerfilComponent } from './usuario-meu-perfil/usuario-meu-perfil.component';


const routes: Routes = [
    { path: 'perfil', component: UsuarioPerfilComponent},
    { path: 'meu-perfil', component: UsuarioMeuPerfilComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRouting { }