import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSharedModule } from '@fuse/shared.module';
import { UsuarioRouting } from './usuario-routing.module';
import { UsuarioPerfilComponent } from './usuario-perfil/usuario-perfil.component';
import { MatDividerModule } from '@angular/material/divider';
import { UsuarioMeuPerfilComponent } from './usuario-meu-perfil/usuario-meu-perfil.component';
import { UsuarioInformacoesComponent } from './usuario-meu-perfil/usuario-informacoes/usuario-informacoes.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatStepperModule } from '@angular/material/stepper';
import { AlterarComponent } from './usuario-meu-perfil/usuario-alterar/usuario-alterar.component';
import { AlterarGuiaComponent } from './usuario-meu-perfil/usuario-guia-alterar/usuario-guia-alterar.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [UsuarioPerfilComponent, UsuarioMeuPerfilComponent, UsuarioInformacoesComponent, AlterarComponent, AlterarGuiaComponent],
    imports: [
        NgxMaskModule.forRoot(),
        FormsModule,
        CommonModule,
        UsuarioRouting,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        FuseSharedModule,
        MatDividerModule,
        MatTabsModule,
        MatButtonModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        FuseSharedModule,
        FuseSharedModule,
        HttpClientModule,
        MatDialogModule,
        MatMomentDateModule,
    ]
})
export class UsuarioModule { }
