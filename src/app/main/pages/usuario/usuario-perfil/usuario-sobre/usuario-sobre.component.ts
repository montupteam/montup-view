import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'profile-about',
  templateUrl: './usuario-sobre.component.html',
  styleUrls: ['./usuario-sobre.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class UsuarioSobreComponent implements OnInit, OnDestroy {
    
    about: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _profileService
     */
    constructor() { this._unsubscribeAll = new Subject(); }
    
    ngOnInit(): void {

    }

    ngOnDestroy(): void {
          this._unsubscribeAll.next();
          this._unsubscribeAll.complete();
    }

}
