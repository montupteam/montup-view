import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject, ViewChild, ElementRef } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { Guia } from 'app/models/guia';
import { GuiaService } from 'app/services/guia.service';
import { Cidade } from 'app/models/cidade';
import { CidadeService } from 'app/services/cidade.service';
import { MontanhaService } from 'app/services/montanha.service';
import { Montanha } from 'app/models/montanha';
import { Router } from '@angular/router';

@Component({
    selector: 'registro-guia',
    templateUrl: './registro-guia.component.html',
    styleUrls: ['./registro-guia.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class RegistroGuiaComponent implements OnInit, OnDestroy {

    idCidade = -1;
    idMontanha = -1;
    guias: Guia[];
    guia: Guia;

    cadastrarGuiaForm: FormGroup;
    form: FormGroup;
    dialogRef: any;

    cidade: Cidade;
    cidades: Cidade[];
    file: File;

    montanha: Montanha;
    montanhas: Montanha[];

    /**
     * Constructor
     * @param {MatDialog} _matDialog
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private guiaservice: GuiaService,
        public dialog: MatDialog,
        private _formBuilder: FormBuilder,
        public _matDialog: MatDialog,
        private cidadeService: CidadeService,
        private montanhaService: MontanhaService,
        private router: Router
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // Private
    private _unsubscribeAll: Subject<any>;

    @ViewChild('certificado', { static: false }) input: ElementRef;

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Reactive Form
        this.form = this._formBuilder.group({
            idCidade: ['', Validators.required],
            idMontanha: ['', Validators.required],
            codigoPostal: ['', [Validators.required, Validators.maxLength(8)]],
            ocupacao: ['', Validators.required],
            descricaoExperiencia: ['', Validators.required],
            descricaoGuia: ['', Validators.required],
            telefone: ['', [Validators.required, Validators.maxLength(20)]],
            temCertificado: [false],
            instagram: ['@', Validators.maxLength(30)]
        });

        this.cidadeService.obterTodosCidade().subscribe(x => {
            this.cidades = x;
        });

        this.montanhaService.obterTodosMontanha().subscribe(x => {
            this.montanhas = x;
        })
    }


    cadastrarGuia() {
        if (this.form.valid) {
            this.guia = Object.assign({}, this.form.value);
            let fileList: FileList = this.input.nativeElement.files;
            if (fileList.length > 0) {
                let file: File = fileList[0];
                this.guiaservice.inserirCertificado(this.guia, file).subscribe(() => {
                    this.router.navigate(['/usuario/meu-perfil']);
                }, error => {
                    console.log(error);
                });
            } 
        }
    }

    /**
     * On destroy
     */
    // tslint:disable-next-line: use-lifecycle-interface
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Finish the horizontal stepper
     */
    finishHorizontalStepper(): void {
        alert('You have finished the horizontal stepper!');
    }

    /**
     * Finish the vertical stepper
     */
    finishVerticalStepper(): void {
        alert('You have finished the vertical stepper!');
    }

    inputFileChange(event) {
        if (event.target.files && event.target.files[0]) {
            const documento = event.target.files[0];

            const formData = new FormData();
            formData.append('documento', documento);
        }
    }

    obterTodasCidades() {
        this.cidadeService.obterTodosCidade().subscribe(
            (x: Cidade[]) => {
                this.cidades = x;
            }, error => {
                console.log(error);
            });
    }

    obterTodasMontanhas() {
        this.montanhaService.obterTodosMontanha().subscribe(
            (x: Montanha[]) => {
                this.montanhas = x;
            }, error => {
                console.log(error);
            });
    }
}
