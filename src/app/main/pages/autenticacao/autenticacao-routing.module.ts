import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './registro-usuario/registro-usuario.component';
import { RegistroGuiaComponent } from './registro-guia/registro-guia.component';
import { MailConfirmComponent } from './mail-confirm/mail-confirm.component';


const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'registro-usuario', component: RegisterComponent },
    { path: 'registro-guia', component: RegistroGuiaComponent },
    { path: 'mail-confirm', component: MailConfirmComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AutenticacaoRoutingModule { }
