import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Usuario } from 'app/models/usuario';
import { AutenticacaoService } from 'app/services/autenticacao.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { element } from 'protractor';

@Component({
    selector: 'registrousuario',
    templateUrl: './registro-usuario.component.html',
    styleUrls: ['./registro-usuario.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class RegisterComponent implements OnInit, OnDestroy {
    registrousuarioForm: FormGroup;

    usuario: Usuario;

    dialogRef: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private snackBar: MatSnackBar,
        private autenticacaoService: AutenticacaoService,
        private router: Router,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.registrousuarioForm = this._formBuilder.group({
            username: ['', Validators.required],
            sobrenome: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.registrousuarioForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registrousuarioForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    cadastrarUsuario() {
        if (this.registrousuarioForm.valid) {
            this.usuario = Object.assign(
                { password: this.registrousuarioForm.get('password').value },
                this.registrousuarioForm.value);
            this.autenticacaoService.inserir(this.usuario).subscribe(
                () => {
                    this.snackBar.open('Cadastro concluído com sucesso', 'Fechar', {
                        duration: 4000,
                    });
                    this.router.navigate(['/autenticacao/mail-confirm']);
                }, error => {
                    const erro = error.error;
                    erro.forEach(element => {
                        switch (element.code) {
                            case 'DuplicateEmail':
                                this.snackBar.open('Cadastro Duplicado', 'Fechar', {
                                    duration: 4000,
                                });
                                break;
                            default:
                                this.snackBar.open('Erro no Cadastro', 'Fechar', {
                                    duration: 4000,
                                });
                        }
                    });
                }
            );
        }
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { passwordsNotMatching: true };
};
