
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {path: '', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)},
    {path: '', loadChildren: () => import('./pesquisa/pesquisa-guia.module').then(m => m.PesquisaGuiaModule)},
    {path: 'mail', loadChildren: () => import('./mail/mail.module').then(x => x.MailModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuiaRoutingModule { }
