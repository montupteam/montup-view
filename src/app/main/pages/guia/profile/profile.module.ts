import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSharedModule } from '@fuse/shared.module';
import { ProfileComponent } from './profile.component';
import { ProfileAboutComponent } from './tabs/about/about.component';
import { ProfileService } from './profile.service';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileContactComponent } from './tabs/contact/contact.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MailComposeDialogComponent } from './tabs/dialog/mensagem-guia/mensagem-guia.component';
import { FuseSidebarModule } from '@fuse/components';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';

const routes = [
    {
        path     : 'perfil-guia/:id',
        component: ProfileComponent
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        ProfileAboutComponent,
        ProfileContactComponent,
        MailComposeDialogComponent,
    ],
    imports     : [
        NgxMaskModule.forRoot(),
        FormsModule,
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        TranslateModule,
        FuseSharedModule,
        FuseSidebarModule
    ],
    providers   : [
        ProfileService
    ],
    entryComponents: [
        MailComposeDialogComponent
    ]
   
})
export class ProfileModule
{
    
}
