import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MailComposeDialogComponent } from './tabs/dialog/mensagem-guia/mensagem-guia.component';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Guia } from 'app/models/guia';
import { ActivatedRoute } from '@angular/router';
import { GuiaService } from 'app/services/guia.service';
import { Usuario } from 'app/models/usuario';
import { environment } from 'environments/environment';


@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileComponent {
   
    guia: Guia = new Guia();
    usuario: Usuario = new Usuario();
    id: number = 0;

    baseUrl =  environment.baseUrl;
    dialogRef: any;
    /**
     * Constructor
     * @param {MatDialog} _matDialog
     */
    constructor(
        private route: ActivatedRoute,
        private guiaservice: GuiaService,
        public _matDialog: MatDialog,
        ) {
    }

    ngOnInit() {
        this.id = +this.route.snapshot.paramMap.get('id');
        this.obterPeloId();
    }

    obterPeloId() {
        this.guiaservice.obterPeloIdGuia(this.id).subscribe((
            _guia: Guia) => {
                this.guia = _guia;
            }, error => {
                console.log(error);
            });
        }

    composeDialog(): void
    {
        this.dialogRef = this._matDialog.open(MailComposeDialogComponent, {
            panelClass: 'mail-compose-dialog'
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Send
                     */
                    case 'send':
                        console.log('new Mail', formData.getRawValue());
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':
                        console.log('delete Mail');
                        break;
                }
            });
    }
}
