import { Component, OnInit, OnChanges } from '@angular/core';
import { GuiaService } from 'app/services/guia.service';
import { Guia } from 'app/models/guia';
import { Usuario } from 'app/models/usuario';

import { environment } from 'environments/environment';
import { element } from 'protractor';


@Component({
    selector: 'pesquisa-guia',
    templateUrl: './pesquisa-guia.component.html',
    styleUrls: ['./pesquisa-guia.component.scss']
})
export class PesquisaGuiaComponent implements OnInit {

    _filtroLista = '';
    guiasFiltrados: any = [];
    guias: any = [];
    guia: Guia;
    usuario: Usuario;
    displayedColumns: string[] = ['foto', 'nome', 'telefone', 'instagram'];
    busca = '';
    idCidade = 0;
    idMontanha = 0;
    familia: Object[];
    urlImagem: string;

    baseUrl =  environment.baseUrl;
    
    constructor(private guiaService: GuiaService) { }

    ngOnInit(): void {
        this.obterTodos();
        console.log(this.familia);
    }

    obterTodos() {
        this.guiaService.obterTodosGuiaFiltro(this.busca, this.idCidade, this.idMontanha).subscribe((
            _guias: Guia[]) => {
            this.guias = _guias;
        }, error => {
            console.log(error);
        });
    }

    //função para receber emit Output do Filho
    reciverFeedback(respostaFilho) {
        this.idCidade = +respostaFilho.idCidade;
        this.idMontanha = +respostaFilho.idMontanha;
        this.obterTodos();
    }
}

