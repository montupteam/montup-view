import { PesquisaGuiaComponent } from './pesquisa-guia.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSidebarModule, FuseDemoModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { GuiaSidebarComponent } from './guia-sidebar/guia-sidebar.component';
import { FuseDemoContentComponent } from '@fuse/components/demo/demo-content/demo-content.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: 'pesquisa-guia',
        component: PesquisaGuiaComponent
    }
];
@NgModule({
    declarations: [
        PesquisaGuiaComponent,
        GuiaSidebarComponent
    ],
    imports: [
        NgxMaskModule.forRoot(),
        FormsModule,
        RouterModule.forChild(routes),
        MatButtonModule,
        MatTabsModule,
        FuseSidebarModule,
        FuseDemoModule,
        FuseSharedModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatTableModule,
        MatPaginatorModule,
        RouterModule,
        MatFormFieldModule,  
        MatDividerModule,
        MatListModule,
        MatButtonModule,
        MatDialogModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatMomentDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
    
    ],
    exports     : [
        FuseDemoContentComponent
    ]
})
export class PesquisaGuiaModule {
}
