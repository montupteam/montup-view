import { Component, Output, EventEmitter } from '@angular/core';
import { CidadeService } from 'app/services/cidade.service';
import { Cidade } from 'app/models/cidade';
import { MontanhaService } from 'app/services/montanha.service';
import { Montanha } from 'app/models/montanha';


@Component({
    selector: 'guia-sidebar',
    templateUrl: './guia-sidebar.component.html',
    styleUrls: ['./guia-sidebar.component.scss']
})
export class GuiaSidebarComponent {

    @Output() respostaFamilia = new EventEmitter();

    idCidade = 0;
    idMontanha = 0;

    cidades: Cidade[];
    montanhas: Montanha[];

    constructor(
        private montanhaService: MontanhaService,
        private cidadeService: CidadeService
    ) { }

    ngOnInit(): void {
        this.atualizarCidades();
        this.atualizarMontanhas();
    }
    atualizarCidades() {
        this.cidadeService.obterTodosCidade().subscribe(x => {
            this.cidades = x;
        });
    }
    atualizarMontanhas() {
        this.montanhaService.obterTodosMontanha().subscribe(x => {
            this.montanhas = x;
        });
    }

    feedback() {
        this.respostaFamilia.emit({
            idCidade: this.idCidade,
            idMontanha: this.idMontanha
        });
    }
}
