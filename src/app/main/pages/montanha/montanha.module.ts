import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MontanhaIndexComponent } from './montanha-index/montanha-index.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSharedModule } from '@fuse/shared.module';
import { MontanhaPesquisaComponent } from './montanha-pesquisa/montanha-pesquisa.component';
import { FuseSidebarModule, FuseDemoModule } from '@fuse/components';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MontanhaPesquisaService } from './montanha-pesquisa/montanha-pesquisa.service';
import { MontanhaPesquisaFolderComponent } from './montanha-pesquisa-folder/montanha-pesquisa-folder.component';
import { AgmCoreModule} from '@agm/core';
import { ProfileService } from '../guia/profile/profile.service';

const routes: Routes = [
    {path: '', loadChildren: () => import('./montanha-perfil/montanha-perfil.module').then(m => m.PerfilMontanhaModule)},


    {
        path     : 'montanha-pesquisa',
        component: MontanhaPesquisaComponent,
        resolve  : {
            academy: MontanhaPesquisaService
        }
    },
    {
        path     : 'courses/:courseId/:courseSlug',
        component: MontanhaPesquisaFolderComponent,
        resolve  : {
            academy: MontanhaPesquisaService
        }
    },
    {
        path      : '**',
        redirectTo: 'courses'
    }
];


@NgModule({
    declarations: [MontanhaIndexComponent, MontanhaPesquisaComponent, MontanhaPesquisaFolderComponent],
    imports: [
        RouterModule.forChild(routes),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDozT6hXZ_OKPurEvh4nuebkMBSjL8TFF8'
        }),
        CommonModule,
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        FuseSharedModule,

        FuseSidebarModule,
        FuseDemoModule,
        MatDividerModule,
        MatTableModule,
        MatPaginatorModule,

        RouterModule,  
        MatListModule,
        MatDialogModule,
        MatMenuModule,
        MatMomentDateModule,
        MatProgressBarModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatSortModule,
        MatToolbarModule,
        MatTooltipModule,
    ], 
    exports: [RouterModule],
    providers: [
        MontanhaPesquisaService, ProfileService
    ]
})
export class MontanhaModule { }
