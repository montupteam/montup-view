import { Component, Input, ChangeDetectorRef, OnInit, ViewChild } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { Observable, interval } from 'rxjs';
import { startWith, take, map } from 'rxjs/operators';
import { Montanha } from 'app/models/montanha';
import { MontanhaService } from 'app/services/montanha.service';
import { CarouselComponent } from './carousel/carousel.component';

@Component({
    selector: 'sobre-montanha',
    templateUrl: './sobre-montanha.component.html',
    styleUrls: ['./sobre-montanha.component.scss'],

})
export class SobreMontanhaComponent {
    @ViewChild('carousel', { static: true }) carousel: CarouselComponent;


    @Input() id: number = 0;
    @Input() montanha: Montanha;

    constructor(private montanhaservice: MontanhaService) { }

    ngOnInit() {

    }

    atualizarFilho() {
        this.carousel.obterTodasImagens();
    }

}
