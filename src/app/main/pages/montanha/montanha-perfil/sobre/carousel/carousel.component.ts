import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { Observable, interval } from 'rxjs';
import { startWith, take, map } from 'rxjs/operators';
import { MontanhaService } from 'app/services/montanha.service';
import { Montanha } from 'app/models/montanha';
import { ImagemMontanha } from 'app/models/imagemMontanha';
import { environment } from 'environments/environment';

@Component({
    selector: 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
    @Input() id = -1;
    imagensMontanhas: ImagemMontanha[];
    montanha: Montanha;
    imgags = [

    ];
    public carouselTileItems: Array<any> = [0];
    public carouselTiles = { 0: [] };
    public carouselTile: NguCarouselConfig = {
        grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
        slide: 1,
        loop: true,
        speed: 250,
        point: {
            visible: true
        },
        load: 2,
        velocity: 0,
        touch: true,
        easing: 'cubic-bezier(0, 0, 0.2, 1)',
        interval: {
            timing: 2500,
            initialDelay: 1000
        },
    };
    constructor(private _cdr: ChangeDetectorRef, private montanhaservice: MontanhaService) { }

    ngAfterViewInit() {
        this._cdr.detectChanges();
    }

    ngOnInit() {
        this.carouselTileItems.forEach(el => {
            this.carouselTileLoad(el);
        });

        this.obterTodasImagens();
    }

    obterTodasImagens() {
        if (this.id != -1) {
            this.montanhaservice.obterTodasImagem(this.id).subscribe((
                _montanhas: ImagemMontanha[]) => {
                this.imagensMontanhas = _montanhas;
                this.imagensMontanhas.forEach(imagem => {
                    this.carouselTiles[0].push(environment.baseUrl + `/imagemmontanha/obterimagem?montanhaId=${imagem.id}`)
                });
            }, error => {
                console.log(error);
            });
        }
    }

    public carouselTileLoad(j) {
        const len = this.carouselTiles[j].length;
    }
}
