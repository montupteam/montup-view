import { Component, OnInit, Input } from '@angular/core';
import { Montanha } from 'app/models/montanha';
import { MontanhaService } from 'app/services/montanha.service';

@Component({
    selector: 'localizacao',
    templateUrl: './localizacao.component.html',
    styleUrls: ['./localizacao.component.scss']
})
export class LocalizacaoComponent {

    @Input() montanha: Montanha;
    @Input() id: number = 0;
    zoom = 11;

    constructor(private motanhaservice: MontanhaService) {
        
    }

    ngOnInit() {
    }

}
