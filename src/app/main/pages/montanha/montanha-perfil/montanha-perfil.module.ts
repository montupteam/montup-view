import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseSharedModule } from '@fuse/shared.module';
import { PerfilMontanhaComponent } from './montanha-perfil.component';
import { SobreMontanhaComponent } from './sobre/sobre-montanha.component';
import { NgModule } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FuseDemoModule, FuseHighlightModule, FuseWidgetModule } from '@fuse/components';
import { NguCarouselModule } from '@ngu/carousel';
import { AgmCoreModule } from '@agm/core';
import { CarouselComponent } from './sobre/carousel/carousel.component';
import { LocalizacaoComponent } from './localizacao/localizacao.component';
import { ProfileTimelineComponent } from './timeline/timeline.component';
import { ProfileService } from './timeline/profile.service';

const routes = [
    {
        path     : 'perfil-montanha/:id', 
        component: PerfilMontanhaComponent
    }
];

@NgModule({
    declarations: [
        PerfilMontanhaComponent, SobreMontanhaComponent, CarouselComponent, LocalizacaoComponent, ProfileTimelineComponent
    ],

    
    imports     : [
        RouterModule.forChild(routes),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDozT6hXZ_OKPurEvh4nuebkMBSjL8TFF8'
        }),
        FuseSharedModule,
        MatTabsModule,
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,    
        MatFormFieldModule,    
        MatInputModule,    
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,       
        MatDividerModule,       
        MatIconModule,
        MatListModule,
        MatMenuModule,       
        MatSlideToggleModule,       
        FuseDemoModule,
        FuseHighlightModule, 
        

        FuseSharedModule,
        FuseWidgetModule,
        NguCarouselModule
        
    ],
    providers   : [
        ProfileService
    ]
})

export class PerfilMontanhaModule
{
    
}

