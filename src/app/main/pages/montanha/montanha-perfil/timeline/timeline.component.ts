import { OnDestroy, OnInit, ViewEncapsulation, Component, Input } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProfileService } from './profile.service';
import { Montanha } from 'app/models/montanha';
import { Comentario } from 'app/models/Comentario';
import { Usuario } from 'app/models/usuario';
import { ComentarioService } from 'app/services/comentario.service';
import { MontanhaService } from 'app/services/montanha.service';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'app/services/usuario-meu-perfil.service';
import { environment } from 'environments/environment';

@Component({
    selector: 'profile-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileTimelineComponent implements OnInit, OnDestroy {
    @Input() id: number = 0;
    @Input() montanha: Montanha;

    baseUrl = environment.baseUrl;

    comentarios: Comentario[];
    comentario: Comentario = new Comentario();

    usuarios: Usuario[];
    usuario: Usuario = new Usuario();

    urlImagem: string;

    montanhas: Montanha[];
    montnaha: Montanha;

    timeline: any;
    dataComentario: Date;

    comentarioTexto: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _profileService
     */
    constructor(
        private _profileService: ProfileService,
        private comentarioservice: ComentarioService,
        private montanhaservice: MontanhaService,
        private route: ActivatedRoute,
        private usuarioservice: UsuarioService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.id = +this.route.snapshot.paramMap.get('id');
        this._profileService.timelineOnChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(timeline => {
                this.timeline = timeline;
            });

        this.obterTodosComentario();
        this.ObterPeloId();
        this.obterUsuarioLogado();
    }


    ObterPeloId() {
        this.montanhaservice.obterPeloIdMontanha(this.id).subscribe(
            (x: Montanha) => {
                this.montanha = x;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    obterUsuarioLogado() {
        this.usuarioservice.obterUsuarioLogado().subscribe((
            _usuario: Usuario) => {
            this.usuario = _usuario;
        }, error => {
            console.log(error);
        });
    }

    inserirComentario() {
        this.comentario.idMontanha = this.id;
        this.comentario.idUsuario = this.usuario.id;
        this.comentarioservice.inserirComentario(this.comentario).subscribe(
            (x: Comentario) => {
                this.obterTodosComentario();
            }, error => {
                console.log(error);
            });
    }
    obterTodosComentario() {

        this.comentarioservice.obterTodosComentario().subscribe(
            (x: Comentario[]) => {
                this.comentarios = x.filter(
                    comentario => comentario.idMontanha === this.id

                );
                console.log(this.comentarios);
            });
    }
}
