import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { Montanha } from 'app/models/montanha';
import { MontanhaService } from 'app/services/montanha.service';
import { ActivatedRoute } from '@angular/router';
import { SobreMontanhaComponent } from './sobre/sobre-montanha.component';
import { Comentario } from 'app/models/Comentario';
import { ComentarioService } from 'app/services/comentario.service';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'montanha-perfil',
    templateUrl: './montanha-perfil.component.html',
    styleUrls: ['./montanha-perfil.component.scss'],
    encapsulation: ViewEncapsulation.None,

})
export class PerfilMontanhaComponent {

    comentario: Comentario = new Comentario();
    montanha: Montanha = new Montanha();
    comentarios: Comentario[];

    cadastrarComentarioForm: FormGroup;
    form: FormGroup;
    id: number = 0;
    @ViewChild('sobre', { static: true }) sobre: SobreMontanhaComponent;


    constructor(
        private montanhaservice: MontanhaService,
        private comentarioservice: ComentarioService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.id = +this.route.snapshot.paramMap.get('id');
        this.sobre.atualizarFilho();
        this.obterPeloId();
        //this.obterTodos();
    }

    obterPeloId() {
        this.montanhaservice.obterPeloIdMontanha(this.id).subscribe((
            _montanha: Montanha) => {
            this.montanha = _montanha;
            this.montanha.latitude = parseFloat(this.montanha.latitude.toString());
            this.montanha.longitude = parseFloat(this.montanha.longitude.toString());
        }, error => {
            console.log(error);
        });
    }

    /*castrarComentario() {
        if (this.form.valid) {
            this.comentario = Object.assign({}, this.form.value);

        }
    }*/

    /*obterTodos() {
        this.comentarioservice.obterTodosComentario().subscribe(
            (x: Comentario[]) => {
                this.comentarios = x;
            }, error => {
                console.log(error);
            });
    }*/
}
