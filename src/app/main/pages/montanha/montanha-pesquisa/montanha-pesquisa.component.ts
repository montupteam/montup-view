import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MontanhaPesquisaService } from './montanha-pesquisa.service';
import { MontanhaService } from 'app/services/montanha.service';
import { Montanha } from 'app/models/montanha';
import { MontanhaCategoriaService } from 'app/services/montanha-categoria.service';
import { MontanhaCategoria } from 'app/models/montanhaCategoria';
import { Categoria } from 'app/models/Categoria';
import { CategoriaService } from 'app/services/categoria.service';
import { MontanhaIndexComponent } from '../montanha-index/montanha-index.component';
import { ImagemMontanhaService } from 'app/services/imagem-montanha.service';
import { ImagemMontanha } from 'app/models/imagemMontanha';
import { NgStyle } from '@angular/common';
import { environment } from 'environments/environment';

@Component({
    selector: 'academy-courses',
    templateUrl: './montanha-pesquisa.component.html',
    styleUrls: ['./montanha-pesquisa.component.scss'],
    animations: fuseAnimations
})

export class MontanhaPesquisaComponent implements OnInit, OnDestroy {

    idCategoria = -1;
    busca = '';

    baseUrl =  environment.baseUrl;
    
    categories: any[];
    courses: any[];
    coursesFilteredByCategory: any[];
    filteredCourses: any[];
    currentCategory: string;
    searchTerm: string;

    categorias: Categoria[];

    categoria: Categoria;

    montanhasfiltradaspelaCategoria: any[];

    montanhas: Montanha[];
    montanha: Montanha;
    montanhacategorias: MontanhaCategoria[];
    montanhacategoria: MontanhaCategoria;

    montanhasfiltradas: any[];



    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AcademyCoursesService} _academyCoursesService
     */
    constructor(
        private _academyCoursesService: MontanhaPesquisaService,
        private categoriaService: CategoriaService,
        private montanhaservice: MontanhaService,
        private montanhacategoriaservice: MontanhaCategoriaService,
        private imagemMontanhaService: ImagemMontanhaService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }



    ngOnInit(): void {
        // Subscribe to courses
        this._academyCoursesService.onCoursesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(montanhas => {
                this.filteredCourses = this.coursesFilteredByCategory = this.montanhas = montanhas;
            });

        this.categoriaService.obterTodosCategoria().subscribe(x => {
            this.categorias = x;
        });
        this.obterTodosFiltro();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    
    obterTodosPeloIdMontanha(): void {
        this.montanhacategoriaservice.obterTodosPeloIdMontanha().subscribe(
            (x: MontanhaCategoria[]) => {
                this.montanhacategorias = x;
                console.log(this.montanhacategorias);
            }, error => {
                console.log(error);
            });
    }

    obterTodosFiltro() {
        this.montanhaservice.obterTodosMontanhaFiltro(this.idCategoria, this.busca).subscribe((
            _montanhas: Montanha[]) => {
            this.montanhas = _montanhas;
            this.filteredCourses = this.montanhas;
        }, error => {
            console.log(error);
        });
    }
    
    obterTodos() {
        this.montanhaservice.obterTodosMontanha().subscribe((
            x: Montanha[]) => {
            this.montanhas = x;
        }, error => {
            console.log(error);
        });
    }

}





