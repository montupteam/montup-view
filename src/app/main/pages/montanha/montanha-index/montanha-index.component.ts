import { Component, OnInit } from '@angular/core';
import { MontanhaService } from 'app/services/montanha.service';
import { Montanha } from 'app/models/montanha';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-montanha-index',
  templateUrl: './montanha-index.component.html',
  styleUrls: ['./montanha-index.component.scss']
})
export class MontanhaIndexComponent implements OnInit {

    montanhas: Montanha[];
    montanha: Montanha;
    modoSalvar = 'post';
    id = -1;
    ultimoId = 0;
    registerForm: FormGroup;
    nome: string;
    altitude: string;
    avaliacao: number;
    descricao: string;
    duracao: string;
    extensao: string;
    idadeMinima: number;
    nivel: string;

  constructor(private service: MontanhaService) { }

  ngOnInit() {
      this.atualizarMontanhas();
  }

  atualizarMontanhas(){
      this.service.obterTodosMontanha().subscribe(x => {
        this.montanhas = x;
      });
  }

  novoEvento(template: any){
      this.modoSalvar = 'post';
      this.openModal(template);
  }

  openModal(template: any) {
    this.registerForm.reset();
    template.show();
  }

  salvarAlteracao(template: any) {
    let montanha = new Montanha();
    if(this.id === -1){
        montanha.id = ++this.ultimoId;
        montanha.nome = this.nome;
        montanha.altitude = this.altitude;
        montanha.avaliacao = this.avaliacao;
        montanha.descricao = this.descricao;
        montanha.duracao = this.duracao;
        montanha.extensao = this.extensao;
        montanha.idadeMinima = this.idadeMinima;
        montanha.nivel = this.nivel;
        this.montanhas.push(montanha);
    }
}

    apagar(montanha) {
        const posicao = this.montanhas.indexOf(montanha);
        this.montanhas.splice(posicao, 1);
      }
    }

