import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { Observable, interval } from 'rxjs';
import { startWith, take, map } from 'rxjs/operators';
import { MontanhaService } from 'app/services/montanha.service';
import { Montanha } from 'app/models/montanha';

@Component({
    selector: 'caurousel-principal',
    templateUrl: './carousel-principal.component.html',
    styleUrls: ['./carousel-principal.component.scss']
})
export class CarouselComponent1 implements OnInit {
    idMontanha = -1;
    montanhas: Montanha[];
    montanha: Montanha;
    imgags = [

    ];
    public carouselTileItems: Array<any> = [0];
    public carouselTiles = {
        0: [
            'assets/images/backgrounds/slider1.jpg',
            'assets/images/backgrounds/slider2.jpg',
            'assets/images/backgrounds/slider3.jpg'
        ]
    };
    public carouselTile: NguCarouselConfig = {
        grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
        slide: 1,
        loop: true,
        speed: 250,
        point: {
            visible: true
        },
        load: 2,
        velocity: 0,
        touch: true,
        easing: 'cubic-bezier(0, 0, 0.2, 1)',
        interval: {
            timing: 2500,
            initialDelay: 1000
        },
    };
    constructor(private _cdr: ChangeDetectorRef, private montanhaservice: MontanhaService) { }

    ngAfterViewInit() {
        this._cdr.detectChanges();
    }

    ngOnInit() {
        this.carouselTileItems.forEach(el => {
            this.carouselTileLoad(el);
        });

      
    }
    

    public carouselTileLoad(j) {
        const len = this.carouselTiles[j].length;

    }
}
