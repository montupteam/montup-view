import { Component, Input } from '@angular/core';
import { Usuario } from 'app/models/usuario';
import { Comentario } from 'app/models/Comentario';
import { UsuarioService } from 'app/services/usuario-meu-perfil.service';
import { ComentarioService } from 'app/services/comentario.service';
import { environment } from 'environments/environment';
import { Montanha } from 'app/models/montanha';
import { ImagemMontanha } from 'app/models/imagemMontanha';
import { ImagemMontanhaService } from 'app/services/imagem-montanha.service';

@Component({
    selector: 'principal',
    templateUrl: './principal.component.html',
    styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent {

    @Input() id: number;
    comentario: Comentario = new Comentario();
    comentarios: Comentario[] = [];

    imagemMontanha: ImagemMontanha = new ImagemMontanha();

    usuario: Usuario;
    usuarios: Usuario[];

    montanha: Montanha;
    montanhas: Montanha[];
    
    urlImagem: string;
    
    baseUrl =  environment.baseUrl;
    constructor(
        private usuarioservice: UsuarioService,
        private comentarioservice: ComentarioService,
        private imagemMontanhaService: ImagemMontanhaService
    ) { }

    ngOnInit() {
        this.usuarioservice.obterTodos().subscribe(x => {
            this.usuarios = x;
        });

        this.obterTodosComentario();
        this.obterPeloIdUsuario();
    }

    obterPeloIdUsuario() {
        this.usuarioservice.obterPeloIdUsuario(this.id).subscribe((
            _usuario: Usuario) => {
            this.usuario = _usuario;

        }, error => {
            console.log(error);
        });
    }

    obterPeloIdComentario() {
        this.comentarioservice.obterPeloIdComentario().subscribe((
            _comentario: Comentario[]) => {
            this.comentarios = _comentario;
        }, error => {
            console.log(error);
        });
    }

    obterTodosComentario() {
        this.comentarioservice.obterTodosComentario().subscribe(
            (x: Comentario[]) => {
                this.comentarios = x;
                console.log(this.comentarios);
            });
    }

}
