import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseDemoModule } from '@fuse/components/demo/demo.module';
import { PrincipalComponent } from './principal.component';
import { FuseSidebarModule, FuseHighlightModule, FuseWidgetModule } from '@fuse/components';
import { TranslateModule } from '@ngx-translate/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NguCarouselModule } from '@ngu/carousel';
import { FormsModule } from '@angular/forms';
import { PrincipalComentarioComponent } from './principal-comentario/principal-comentario.component';
import { CarouselComponent1 } from './CarroselPrincipal/carousel-principal.component';

const routes: Routes = [
    // Carded
    {
        path     : '',
        component: PrincipalComponent
    }
];

@NgModule({
    declarations: [    
        PrincipalComponent, CarouselComponent1, PrincipalComentarioComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        FuseSharedModule,
        MatTabsModule,
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MatDividerModule,
        MatIconModule,
        MatListModule,
        MatMenuModule,
        MatSlideToggleModule,
        FuseDemoModule,
        FuseHighlightModule,
        FuseSharedModule,
        FuseWidgetModule,
        NguCarouselModule,
        FuseSidebarModule,
        FuseSharedModule,
        FuseDemoModule,
        FormsModule,
    ]
})
export class PrincipalModule
{
}
