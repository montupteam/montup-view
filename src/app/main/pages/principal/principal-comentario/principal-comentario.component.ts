import { Component, OnInit, Input } from '@angular/core';
import { Comentario } from 'app/models/Comentario';

@Component({
  selector: 'app-principal-comentario',
  templateUrl: './principal-comentario.component.html',
  styleUrls: ['./principal-comentario.component.scss']
})
export class PrincipalComentarioComponent implements OnInit {

    @Input() comentario: Comentario;
    @Input() index: number = 0;

  constructor() { }

  ngOnInit() {
  }

}
