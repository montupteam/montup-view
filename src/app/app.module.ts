import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppInsertImageDialog } from './main/pages/usuario/usuario-meu-perfil/usuario-meu-perfil.component';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { PesquisaGuiaModule } from './main/pages/guia/pesquisa/pesquisa-guia.module';
import { NguCarouselModule } from '@ngu/carousel';
import { TranslateModule } from '@ngx-translate/core';
import { FakeDbService } from './fake-db/fake-db.service';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FuseModule } from '@fuse/fuse.module';
import { fuseConfig } from './fuse-config';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { LayoutModule } from '@angular/cdk/layout';
import { SampleModule } from './main/sample/sample.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AutenticaoInterceptor } from './autenticacao/autenticacao.interceptor';
import { VerticalLayout1Module } from './layout/vertical/layout-1/layout-1.module';

@NgModule({
    declarations: [
        AppComponent,
        AppInsertImageDialog,
    ],
    imports: [
        BrowserModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDozT6hXZ_OKPurEvh4nuebkMBSjL8TFF8'
        }),
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        PesquisaGuiaModule,
        NguCarouselModule,
        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        VerticalLayout1Module,
        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        HttpClientModule,
        // App modules
        LayoutModule,
        SampleModule,
        BrowserModule,
        FormsModule
    ],
    exports: [RouterModule],
    bootstrap: [
        AppComponent
    ],
    entryComponents: [
        AppInsertImageDialog
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AutenticaoInterceptor, multi: true}
    ]
})
export class AppModule {
}
