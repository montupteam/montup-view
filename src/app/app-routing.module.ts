import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutenticacaoGuard } from './autenticacao/autenticacao.guard';

const routes: Routes = [    
    { path: 'autenticacao', loadChildren: () => import('./main/pages/autenticacao/autenticacao.module').then(m => m.AutenticacaoModule)},
    { path: 'guia', loadChildren: () => import('./main/pages/guia/guia.module').then(x => x.GuiaModule), canActivate: [AutenticacaoGuard]},
    { path: 'montanha', loadChildren: () => import('./main/pages/montanha/montanha.module').then(x => x.MontanhaModule), canActivate: [AutenticacaoGuard]},
    { path: 'usuario', loadChildren: () => import('./main/pages/usuario/usuario.module').then(x => x.UsuarioModule), canActivate: [AutenticacaoGuard]},
    { path: 'sobrenos', loadChildren: () => import('./main/pages/sobrenos/sobrenos.module').then(m => m.SobreNosModule), canActivate: [AutenticacaoGuard]},


    { path: 'principal', loadChildren: () => import('./main/pages/principal/principal.module').then(m => m.PrincipalModule), canActivate: [AutenticacaoGuard]},


];
@NgModule({
    imports: [RouterModule.forRoot(routes, {
        useHash: true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
