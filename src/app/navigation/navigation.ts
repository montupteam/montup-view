import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'Menu',
        title    : 'Menu',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'dashboard',
                title    : 'Home',
                type     : 'item',
                icon     : 'home',
                url: '/principal'
            },

            {
                id       : 'guia',
                title    : 'Guia',
                type     : 'collapsable',
                icon     : 'person_pin',
                children : [
                    {
                        id: 'guia-pesquisa',
                        title: 'Pesquisar Guias',
                        type: 'item',
                        url: '/guia/pesquisa-guia',  
                    }
                ]                                    
            },
            {
                id       : 'montanha',
                title    : 'Montanha',
                type     : 'collapsable',
                icon     : 'nature_people',
                children : [
                    {
                        id: 'montanha-pesquisa',
                        title: 'Pesquisar Montanhas',
                        type: 'item',
                        url: '/montanha/montanha-pesquisa'
                    }
                ]          
            },
            {
                id       : 'sobrenos',
                title    : 'Sobre nós',
                type     : 'item',
                icon     : 'help',
                url      : '/sobrenos',             
            }       
        ]
    }
];
