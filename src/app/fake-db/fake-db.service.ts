import { Injectable } from '@angular/core';
import { AcademyFakeDb } from './MontanhaPesquisaFakeDb';
import { MailFakeDb } from './mail';
import { ProfileFakeDb } from './profile';

@Injectable({
  providedIn: 'root'
})

export class FakeDbService {

    createDb(): any
    {
        return {

            // Academy
            'academy-categories': AcademyFakeDb.categories,
            'academy-courses'   : AcademyFakeDb.courses,
            'academy-course'    : AcademyFakeDb.course,

             // Mail
             'mail-mails'  : MailFakeDb.mails,
             'mail-folders': MailFakeDb.folders,
            
 
            // Profile
            'profile-timeline'     : ProfileFakeDb.timeline
        };
    }

}
