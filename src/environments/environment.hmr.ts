export const environment = {
    production: false,
    hmr       : true,
    baseUrl: 'https://localhost:5001/api',
    pusher: {
      key: 'f63cf0218426a28c647e',
      cluster: 'us2',
    }
};
