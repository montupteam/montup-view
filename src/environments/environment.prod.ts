export const environment = {
    production: true,
    hmr       : false,
    baseUrl: 'https://montup-server.azurewebsites.net/api',
    pusher: {
      key: 'f63cf0218426a28c647e',
      cluster: 'us2',
    }
};
