import { Component } from '@angular/core';

export interface Food {
    value: string;
    viewValue: string;
  }



@Component({
        selector: 'fuse-demo-sidebar',
        templateUrl: './demo-sidebar.component.html',
        styleUrls: ['./demo-sidebar.component.scss']
    })
export class FuseDemoSidebarComponent {
    /**
     * Constructor
     */
    constructor() {
    }
}
export class SelectValueBindingExample {
  selected = 'option2';
}
